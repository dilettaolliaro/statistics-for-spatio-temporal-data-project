---
title: "Project Code"
author: "Diletta Olliaro"
date: "5/13/2020"
output: pdf_document
---
```{r}
#needed library
library(zoo)
library(xts)
library(car)
library(forecast)
library(fpp2)
```

```{r}
# we read the dataset
tides <- read.csv("dailymax.csv")
# we create the sequence of dates of our timeline
days <-seq(from=as.Date("1983-1-01"), length.out = nrow(tides), by="day")
# we create the 'xts' object that will represent our time series
ts.tides <- xts(tides$Level, order.by = days)
#xts objects have frequency set to daily by default, so we change it manually
attr(ts.tides, 'frequency') <- 28
plot(ts.tides)
```

# A First Exploratory Analysis

```{r}
# we create the first subset of five years
from1983to1988 <- window(ts.tides, start = "1983-01-01", end="1988-12-31")
# we create the second subset of five years
from1998to2003 <- window(ts.tides, start = "1998-01-01", end="2002-12-31")
# we create the third subset of five years
from2013to2018 <- window(ts.tides, start = "2013-01-01", end="2018-12-31")
# we compute the mean for each subset
c(mean(from1983to1988), mean(from1998to2003), mean(from2013to2018))
```

```{r}
# decomment the following line of code to have the plot exactly 
# as in the report

#par(mfrow=c(3,1))

# we plot the first subset
plot(from1983to1988)
# we plot the second subset
plot(from1998to2003)
# we plot the third subset
plot(from2013to2018)
```

```{r}
# we create the first subset of three months
subset1 <- window(ts.tides, start = "1987-01-01", end="1987-03-31")
# we create the second subset of three months
subset2 <- window(ts.tides, start = "2002-05-01", end="2002-07-31")
# we create the third subset of three months
subset3 <- window(ts.tides, start = "2015-09-01", end="2015-11-30")

# decomment the following line of code to have the plot exactly 
# as in the report

#par(mfrow=c(3,1))

# plot the first subset
plot(subset1)
# plot the second subset
plot(subset2)
# plot the third subset
plot(subset3)
```

# Decompostion

```{r}
# we assume the model to be additive and we explain why in the report
decomposition <- decompose(as.ts(ts.tides), "additive")
plot(decomposition)
```

First applied filter to extract trend (MA)
```{r}
# smoothing parameter
p <- 14
# we define the symmetric weights
weights <- rep(1/(2*p+1), times=2*p+1)
# we extract the trend using function filter
# sides = 2 means vlaues are centered at lag 0, meaning around the observations itself
trend <- filter(ts.tides, sides=2, filter=weights)
```

```{r}
# we plot the found trend
plot(trend)
# we plot the "reference" trend
lines(decomposition$trend, col="red")
```

Second filter applied to extract the \textit{seasonal trend}
```{r}
# frequency is given by 28 
f <- 28
# we define the vector of weights and divide them for the frequency
weights.s <- c(0.5, rep(1,f-1), 0.5)/f
# we extract the trend that considers also the seasonal pattern
trend.s <- filter(ts.tides, side=2, filter=weights.s)
# we plot the new trend and we notice is not that different from the previous one
plot(trend.s, col="orange", ylab="seasonal trend")
# we compute Mean Absolute Percentage Error (MAPE) 
# between the two trend to show they are almost the same 
# meaning the seasonality we choose is not that relevant
accuracy(trend,trend.s)[1,5]
```


We now look for the main pattern of our seasonal component.
```{r}
# we detrend our series
detrended <- as.ts(ts.tides)-trend.s
# we fit our ts in a matrix divided in 28 rows/periods
# t() takes the transpose matrix
matrix <- t(matrix(data = detrended, nrow = 28))
# we take the mean of each column i.e. of each period
seasonality <- as.ts(colMeans(matrix, na.rm = T))
```


```{r}
# we plot the found average seasonal pattern
plot(seasonality, ylab="figure")
# we plot on it the "reference" figure
lines(decomposition$figure, col="chocolate1")
```



```{r}
# we retrieve residuals removing both seasonality and trend
residuals <- as.ts(ts.tides)-trend.s-rep(seasonality, 470)[1:13149]
```


```{r}
# we plot the found residuals
plot(residuals)
# we plot the "reference" residuals
lines(decomposition$random, col="antiquewhite3")
```

# Parameter Estimation

```{r}
# we show we do not have zero mean
mean(ts.tides)
```

```{r}
# plot of the autocorrelation function of our ts
acf(ts.tides, main = "Autocorrelation Function", lag.max = 280)
# plot of the partial autocorrelation function of our ts
pacf(ts.tides, main = "Partial Autocorrelation Function", lag.max = 280)
# first non-seasonal difference of our ts
fd <- na.omit(diff((as.ts(ts.tides))))
# its plot
plot(fd, ylab="Tide Level (cm)")

# decomment the following line of code to have the plot exactly 
# as in the report

# par(mfrow=c(1,2))


# its autocorrelation function graph
acf(fd, lag.max = 120, main = "ACF of the differenced series")# and its partial autocorrelation function graph
pacf(fd, lag.max = 120, main = "PACF of the differenced series")
# and its mean
mean(fd)
# first seasonal difference of our ts
fsd <- na.omit(diff((as.ts(ts.tides)), lag = 28))
# its plot
plot(fsd, ylab="Tide Level (cm)")
# and its mean
mean(fsd)
```

Fit1 - Arima(1,1,1)(0,0,1)[28]
```{r}
# we fit the model
fit1 <- Arima(ts.tides, order=c(1,1,1), seasonal=c(0,0,1))
# we look at the parameters and at the information criteria
fit1
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit1$residuals,type="Box-Pierce")
Box.test(fit1$residuals,type="Ljung")
# we check the ACF function
acf(fit1$residuals)
# we check the PACF function
pacf(fit1$residuals)
```

Fit2 - Arima(2,1,1)(0,0,1)[28]
```{r}
# we fit the model
fit2 <- Arima(ts.tides, order=c(2,1,1), seasonal=c(0,0,1))
# we look at the parameters and at the information criteria
fit2
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit2$residuals,type="Box-Pierce")
Box.test(fit2$residuals,type="Ljung")
# we check the ACF function
acf(fit2$residuals)
# we check the PACF function
pacf(fit2$residuals)
```

Fit3 - Arima(3,1,1)(0,0,1)[28]
```{r}
# we fit the model
fit3 <- Arima(ts.tides, order=c(3,1,1), seasonal=c(0,0,1))
# we look at the parameters and at the information criteria
fit3
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit3$residuals,type="Box-Pierce")
Box.test(fit3$residuals,type="Ljung")
# we check the ACF function
acf(fit3$residuals)
# we check the PACF function
pacf(fit3$residuals)
```

Fit4 - Arima(4,1,1)(0,0,1)[28]
```{r}
# we fit the model
fit4 <- Arima(ts.tides, order=c(4,1,1), seasonal=c(0,0,1))
# we look at the parameters and at the information criteria
fit4
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit4$residuals,type="Box-Pierce")
Box.test(fit4$residuals,type="Ljung")
# we check the ACF function
acf(fit4$residuals)
# we check the PACF function
pacf(fit4$residuals)
```

Fit5 - Arima(2,1,1)(0,0,2)[28]
```{r}
# we fit the model
fit5 <- Arima(ts.tides, order=c(2,1,1), seasonal=c(0,0,2))
# we look at the parameters and at the information criteria
fit5
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit5$residuals,type="Box-Pierce")
Box.test(fit5$residuals,type="Ljung")
# we check the ACF function
acf(fit5$residuals)
# we check the PACF function
pacf(fit5$residuals)
```

Fit6 - Arima(3,1,1)(0,0,2)[28]
```{r}
# we fit the model
fit6 <- Arima(ts.tides, order=c(3,1,1), seasonal=c(0,0,2))
# we look at the parameters and at the information criteria
fit6
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit6$residuals,type="Box-Pierce")
Box.test(fit6$residuals,type="Ljung")
# we check the ACF function
acf(fit6$residuals)
# we check the PACF function
pacf(fit6$residuals)
```


Fit7 - Arima(2,1,1)(1,0,1)[28]
```{r}
# we fit the model
fit7 <- Arima(ts.tides, order=c(2,1,1), seasonal=c(1,0,1))
# we look at the parameters and at the information criteria
fit7
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit7$residuals,type="Box-Pierce")
Box.test(fit7$residuals,type="Ljung")
# we check the ACF function
acf(fit7$residuals)
# we check the PACF function
pacf(fit7$residuals)
```




Fit8 - Arima(2,1,1)(1,0,2)[28]
```{r}
# we fit the model
fit8 <- Arima(ts.tides, order=c(2,1,1), seasonal=c(1,0,2))
# we look at the parameters and at the information criteria
fit8
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit8$residuals,type="Box-Pierce")
Box.test(fit8$residuals,type="Ljung")
# we check the ACF function
acf(fit8$residuals)
# we check the PACF function
pacf(fit8$residuals)
```





Fit9 - Arima(3,1,1)(0,1,1)[28]
```{r}
# we fit the model
fit9 <- Arima(ts.tides, order=c(3,1,1), seasonal=c(0,1,1))
# we look at the parameters and at the information criteria
fit9
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit9$residuals,type="Box-Pierce")
Box.test(fit9$residuals,type="Ljung")
# we check the ACF function
acf(fit9$residuals)
# we check the PACF function
pacf(fit9$residuals)
```



Fit10 - Arima(4,1,1)(0,1,1)[28]
```{r}
# we fit the model
fit10 <- Arima(ts.tides, order=c(4,1,1), seasonal=c(0,1,1))
# we look at the parameters and at the information criteria
fit10
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit10$residuals,type="Box-Pierce")
Box.test(fit10$residuals,type="Ljung")
# we check the ACF function
acf(fit10$residuals)
# we check the PACF function
pacf(fit10$residuals)
```


Fit11 - Arima(2,1,1)(0,1,2)[28]
```{r}
# we fit the model
fit11 <- Arima(ts.tides, order=c(2,1,1), seasonal=c(0,1,2))
# we look at the parameters and at the information criteria
fit11
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit11$residuals,type="Box-Pierce")
Box.test(fit11$residuals,type="Ljung")
# we check the ACF function
acf(fit11$residuals)
# we check the PACF function
pacf(fit11$residuals)
```


Fit12 - Arima(2,1,1)(1,1,2)[28]
```{r}
# we fit the model
fit12 <- Arima(ts.tides, order=c(2,1,1), seasonal=c(1,1,2))
# we look at the parameters and at the information criteria
fit12
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit12$residuals,type="Box-Pierce")
Box.test(fit12$residuals,type="Ljung")
# we check the ACF function
acf(fit12$residuals)
# we check the PACF function
pacf(fit12$residuals)
```

At this point we split our time series in training and test set
```{r}
# training set composed by 30 years of observations
training <- subset(as.ts(ts.tides), end=length(ts.tides)-2191)
# test set composed by 6 years of observations (almost 20%)
test <- subset(as.ts(ts.tides), start=length(ts.tides)-2190)
```


And we fit the two best model on the training set so we can choose the best one according to normality of the residuals, how much the residuals are close to white noise and also according to how they forecast.

We fit model fit5 on the training set
```{r}
# we fit the model
fit5tr <- Arima(training, order=c(2,1,1), seasonal=c(0,0,2))
# we look at the parameters and at the information criteria
fit5tr

# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit5tr$residuals,type="Box-Pierce")
Box.test(fit5tr$residuals,type="Ljung")

# we compute the density function of the residuals 
d5 <- density(fit5tr$residuals, na.rm=T)
# we plot the histogram of the residuals
hist(fit5tr$residuals, freq=F, ylim=range(d5$y))
lines(d5, col="red")
# we check normality of the residuals
qqPlot(fit5tr$residuals)

# we check the ACF function
acf(fit5tr$residuals)
# we check the ACF function
pacf(fit5tr$residuals)
```

```{r}
# we forecast the last 6 years of observations
f5 <- forecast(fit5tr, h=2190)
# we plot them
plot(f5, ylab="Tide level (cm)")
# and we compare them with the observations of the test set
lines(test, col="cornsilk4")
# mean contains the forecasts
lines(f5$mean, col="darkred", lwd=2)
c <- c("cornsilk4", "darkred")
legend("topleft", c("test set", "forecasts"), col=c, lty=c(1,1), lwd = c(2,2))
# check accuracy of the forecast
fit5tr.acc <- accuracy(f5,test)[2,c(2,3,5,6)]
fit5tr.acc
```

We fit model fit12 on the training set
```{r}
# we fit the model
fit12tr <- Arima(training, order=c(2,1,1), seasonal=c(1,1,2))
# we look at the parameters and at the information criteria
fit12tr
```

```{r}
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit12tr$residuals,type="Box-Pierce")
Box.test(fit12tr$residuals,type="Ljung")

# we compute the density function of the residuals 
d12 <- density(fit12tr$residuals, na.rm=T)
# we plot the histogram of the residuals
hist(fit12tr$residuals, freq=F, ylim=range(d12$y))
lines(d12, col="red")
# we check normality of the residuals
qqPlot(fit12tr$residuals)

# we check the ACF function
acf(fit12tr$residuals)
# we check the ACF function
pacf(fit12tr$residuals)

# we forecast the last 6 years of observations
f12 <- forecast(fit12tr, h=2190)
# we plot them
plot(f12, ylab="Tide level (cm)")
# and we compare them with the observations of the test set
lines(test, col="cornsilk4")
# mean contains the forecasts
lines(f12$mean, col="darkred", lwd=2)
c <- c("cornsilk4", "darkred")
legend("topleft", c("test set", "forecasts"), col=c, lty=c(1,1), lwd = c(2,2))
# check accuracy of the forecast
fit12tr.acc <- accuracy(f12,test)[2,c(2,3,5,6)]
fit12tr.acc
```
```{r}
# to highlight wigliness
x <- c((length(training)-length(test)/4)/28, (length(training)+length(test))/28)
y <- c(35, 110)
plot(f12, ylab="Tide level (cm)", main="fit12 forecasts", xlim=x, ylim = y)

lines(test, col="cornsilk4")

lines(f12$mean, col="darkred", lwd=2)
```






```{r}
# we choose the lambda parameter for the Box-Cox transformation
lambda <- BoxCox.lambda(ts.tides)
lambda
```

```{r}
library(MASS)
boxcoxtrans <- BoxCox(as.ts(ts.tides), lambda)
```

```{r}
# plot of the autocorrelation function of our transformed ts
acf(boxcoxtrans, main = "Autocorrelation Function", lag.max = 280)
# plot of the partial autocorrelation function of our transformed ts
pacf(boxcoxtrans, main = "Partial Autocorrelation Function", lag.max = 280)
# first non-seasonal difference of our transformed ts
fd <- na.omit(diff((boxcoxtrans)))
# its plot
plot(fd, ylab="Tide Level (cm)")
# its autocorrelation function graph
acf(fd, lag.max = 120, main = "ACF of the transformed differenced series")
# and its partial autocorrelation function graph
pacf(fd, lag.max = 120, main = "PACF of the transformed differenced series")
# and its mean
mean(fd)
# first seasonal difference of our transformed ts
fsd <- na.omit(diff((boxcoxtrans), lag = 28))
# its plot
plot(fsd, ylab="Tide Level (cm)")
# and its mean
mean(fsd)
```

Fit1 - Arima(1,1,1)(0,0,1)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit1cox <- Arima(ts.tides, order=c(1,1,1), seasonal=c(0,0,1), lambda = lambda)
# we look at the parameters and at the information criteria
fit1cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit1cox$residuals,type="Box-Pierce")
Box.test(fit1cox$residuals,type="Ljung")
# we check the ACF function
acf(fit1cox$residuals)
# we check the PACF function
pacf(fit1cox$residuals)
```

Fit2 - Arima(2,1,1)(0,0,1)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit2cox <- Arima(ts.tides, order=c(2,1,1), seasonal=c(0,0,1), lambda = lambda)
# we look at the parameters and at the information criteria
fit2cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit2cox$residuals,type="Box-Pierce")
Box.test(fit2cox$residuals,type="Ljung")
# we check the ACF function
acf(fit2cox$residuals)
# we check the PACF function
pacf(fit2cox$residuals)
```

Fit3 - Arima(3,1,1)(0,0,1)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit3cox <- Arima(ts.tides, order=c(3,1,1), seasonal=c(0,0,1), lambda = lambda)
# we look at the parameters and at the information criteria
fit3cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit3cox$residuals,type="Box-Pierce")
Box.test(fit3cox$residuals,type="Ljung")
# we check the ACF function
acf(fit3cox$residuals)
# we check the PACF function
pacf(fit3cox$residuals)
```

Fit4 - Arima(4,1,1)(0,0,1)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit4cox <- Arima(ts.tides, order=c(4,1,1), seasonal=c(0,0,1), lambda = lambda)
# we look at the parameters and at the information criteria
fit4cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit4cox$residuals,type="Box-Pierce")
Box.test(fit4cox$residuals,type="Ljung")
# we check the ACF function
acf(fit4cox$residuals)
# we check the PACF function
pacf(fit4cox$residuals)
```

Fit5 - Arima(2,1,1)(0,0,2)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit5cox <- Arima(ts.tides, order=c(2,1,1), seasonal=c(0,0,2), lambda = lambda)
# we look at the parameters and at the information criteria
fit5cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit5cox$residuals,type="Box-Pierce")
Box.test(fit5cox$residuals,type="Ljung")
# we check the ACF function
acf(fit5cox$residuals)
# we check the PACF function
pacf(fit5cox$residuals)
```

Fit6 - Arima(3,1,1)(0,0,2)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit6cox <- Arima(ts.tides, order=c(3,1,1), seasonal=c(0,0,2), lambda = lambda)
# we look at the parameters and at the information criteria
fit6cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit6cox$residuals,type="Box-Pierce")
Box.test(fit6cox$residuals,type="Ljung")
# we check the ACF function
acf(fit6cox$residuals)
# we check the PACF function
pacf(fit6cox$residuals)
```


Fit7 - Arima(2,1,1)(1,0,1)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit7cox <- Arima(ts.tides, order=c(2,1,1), seasonal=c(1,0,1), lambda = lambda)
# we look at the parameters and at the information criteria
fit7cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit7cox$residuals,type="Box-Pierce")
Box.test(fit7cox$residuals,type="Ljung")
# we check the ACF function
acf(fit7cox$residuals)
# we check the PACF function
pacf(fit7cox$residuals)
```




Fit8 - Arima(2,1,1)(1,0,2)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit8cox <- Arima(ts.tides, order=c(2,1,1), seasonal=c(1,0,2), lambda = lambda)
# we look at the parameters and at the information criteria
fit8cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit8cox$residuals,type="Box-Pierce")
Box.test(fit8cox$residuals,type="Ljung")
# we check the ACF function
acf(fit8cox$residuals)
# we check the PACF function
pacf(fit8cox$residuals)
```





Fit9 - Arima(3,1,1)(0,1,1)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit9cox <- Arima(ts.tides, order=c(3,1,1), seasonal=c(0,1,1), lambda = lambda)
# we look at the parameters and at the information criteria
fit9cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit9cox$residuals,type="Box-Pierce")
Box.test(fit9cox$residuals,type="Ljung")
# we check the ACF function
acf(fit9cox$residuals)
# we check the PACF function
pacf(fit9cox$residuals)
```



Fit10 - Arima(4,1,1)(0,1,1)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit10cox <- Arima(ts.tides, order=c(4,1,1), seasonal=c(0,1,1), lambda = lambda)
# we look at the parameters and at the information criteria
fit10
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit10cox$residuals,type="Box-Pierce")
Box.test(fit10cox$residuals,type="Ljung")
# we check the ACF function
acf(fit10cox$residuals)
# we check the PACF function
pacf(fit10cox$residuals)
```


Fit11 - Arima(2,1,1)(0,1,2)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit11cox <- Arima(ts.tides, order=c(2,1,1), seasonal=c(0,1,2), lambda = lambda)
# we look at the parameters and at the information criteria
fit11cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit11cox$residuals,type="Box-Pierce")
Box.test(fit11cox$residuals,type="Ljung")
# we check the ACF function
acf(fit11cox$residuals)
# we check the PACF function
pacf(fit11cox$residuals)
```


Fit12 - Arima(2,1,1)(1,1,2)[28] \textit{on Box-Cox transformed data}
```{r}
# we fit the model
fit12cox <- Arima(ts.tides, order=c(2,1,1), seasonal=c(1,1,2), lambda = lambda)
# we look at the parameters and at the information criteria
fit12cox
# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit12cox$residuals,type="Box-Pierce")
Box.test(fit12cox$residuals,type="Ljung")
# we check the ACF function
acf(fit12cox$residuals)
# we check the PACF function
pacf(fit12cox$residuals)
```




We fit model fit5 on the Box-Cox transformed training set
```{r}
# we fit the model
fit5trCox <- Arima(training, order=c(2,1,1), seasonal=c(0,0,2), lambda=lambda)
# we look at the parameters and at the information criteria
fit5trCox

# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit5trCox$residuals,type="Box-Pierce")
Box.test(fit5trCox$residuals,type="Ljung")

# we compute the density function of the residuals 
d5Cox <- density(fit5trCox$residuals, na.rm=T)
# we plot the histogram of the residuals
hist(fit5trCox$residuals, freq=F, ylim=range(d5Cox$y))
lines(d5Cox, col="red")
# we check normality of the residuals
qqPlot(fit5trCox$residuals)

# we check the ACF function
acf(fit5trCox$residuals)
# we check the ACF function
pacf(fit5trCox$residuals)

# we forecast the last 6 years of observations
f5Cox <- forecast(fit5trCox, h=2190)
# we plot them
plot(f5Cox, ylab="Tide level (cm)")
# and we compare them with the observations of the test set
lines(test, col="cornsilk4")
# mean contains the forecasts
lines(f5Cox$mean, col="darkred", lwd=2)
#we add a legend
c <- c("cornsilk4", "darkred")
legend("topleft", c("test set", "forecasts"), col=c, lty=c(1,1), lwd = c(2,2))

# check accuracy of the forecast
fit5trCox.acc <- accuracy(f5Cox,test)[2,c(2,3,5,6)]
fit5trCox.acc
```

We fit model fit8 on the training set
```{r}
# we fit the model
fit8trCox <- Arima(training, order=c(2,1,1), seasonal=c(1,0,2), lambda=lambda)
# we look at the parameters and at the information criteria
fit8trCox

# we check the p-value (the higher it is closer we are to have white noise residuals)
Box.test(fit8trCox$residuals,type="Box-Pierce")
Box.test(fit8trCox$residuals,type="Ljung")

# we compute the density function of the residuals 
d8Cox <- density(fit8trCox$residuals, na.rm=T)
# we plot the histogram of the residuals
hist(fit8trCox$residuals, freq=F, ylim=range(d8Cox$y))
lines(d8Cox, col="red")
# we check normality of the residuals
qqPlot(fit8trCox$residuals)

# we check the ACF function
acf(fit8trCox$residuals)
# we check the ACF function
pacf(fit8trCox$residuals)

# we forecast the last 6 years of observations
f8Cox <- forecast(fit8trCox, h=2190)
# we plot them
plot(f8Cox, ylab="Tide level (cm)")
# and we compare them with the observations of the test set
lines(test, col="cornsilk4")
# mean contains the forecasts
lines(f8Cox$mean, col="darkred", lwd=2)
c <- c("cornsilk4", "darkred")
legend("topleft", c("test set", "forecasts"), col=c, lty=c(1,1), lwd = c(2,2))

# check accuracy of the forecast
fit8trCox.acc <- accuracy(f8Cox,test)[2,c(2,3,5,6)]
fit8trCox.acc
```







