\documentclass[12pt]{article}
\renewcommand{\baselinestretch}{1.15}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[dvipsnames]{xcolor}
\usepackage[T1]{fontenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{qtree}
\usepackage{listings}
\usepackage{tikz}
\usepackage{centernot}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{mdframed}
\usepackage{verbatim}
\usepackage{fancyvrb}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{float}
\usepackage{pxfonts}
\usepackage{pifont}
\usepackage{enumitem}
\usepackage{adforn}
\usepackage{calligra}
\usepackage{bbding}
\usepackage{textcomp}
\usepackage{tikz}
\usepackage{geometry}
\usepackage{tablefootnote}
%\setlength{\footnotesep}{1.\baselineskip}%



\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{lastpage} % last page ref
\usepackage{booktabs}
\usepackage{array}
\usepackage{filecontents}


\pagestyle{fancy}
\fancyhf{}


%\usepackage{ntheorem}
\usetikzlibrary{shapes}
\usetikzlibrary{arrows}
\usetikzlibrary{calc}

 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }
 
%\setcounter{secnumdepth}{4}
%\setcounter{tocdepth}{4}

\begin{document}
\pagenumbering{gobble}

\date{A.A. 2019/2020}
\author{Diletta Olliaro\\\ \\ Dipartimento di Scienze Ambientali, Informatica e Statistica\\ Università Ca'Foscari Venezia}
\title{Tides Time Series Analysis}
\maketitle
%\doclicenseImage
%\par\medskip\noindent
%\doclicenseText
\newpage
\tableofcontents
\newpage
%\fancyfoot[RO,LE]{Page \thepage \hspace{1pt} of \pageref{LastPage}}
\pagenumbering{arabic}

\section{Introduction}

\paragraph*{Motivation.} \textit{"Venice has been called the floating city when, actually, it is sinking"}\cite{ar}. These are the words written in a The Verge article after the November, 2019 high tide in Venice; one of the worst recent moments for the Venetian Lagoon. To better understand this phenomenon we shall travel backward in the history of our beloved city. We have written testimonies of flooding in the Venetian Lagoon since 589, even though regular scientific record-keeping actually started in 1871 with the installation of the first modern marigraph for regular tide monitoring\cite{wk}. \\

This phenomenon is largely studied for two main reasons, the first one is its economic and social impact, high tides as the one we witnessed in November or the ones of which we have historical records create a different range of critical situations: firstly the safety of people living in the city, then the flooding of commercial activities and houses and also the general damaging of the city. The second reason why this phenomenon is so highly studied is that we are talking about a really complex phenomenon that depends on multiple factors such as meteorological, geophysical and astronomical components. This complexity reflects on our capability to forecast high tides values, being able to predict these kind of events would have an outstanding impact on the prevention modalities and on the welfare of the city. Moreover if we consider the natural sinking of the soil level and the progressive rise of sea levels we could see why this is an urgent topic to be treated.


\paragraph*{Objectives and initial problems.} The main goal of this project was to analyse the time series created on the hourly observations of 36 years (from 1983 to 2018) of the level of the tides in Venice, in particular on the measurements taken in Punta della Salute (from the side of the Giudecca Canal). 

The historical records were found in the web page of the city of Venice, in particular at this \textcolor{blue}{\href{https://www.comune.venezia.it/node/6214}{source}}\cite{ven}; data were divided in different .csv files, one per each year, however for our analysis these datasets have been joined creating a unique time series. \\



The analysis should have served to perform some proper forecast, so that maybe we would have been able to predict the high tide that hit the Venetian lagoon on 2019, November 12.
However we encountered some problems and we had to change objective accordingly. We had two main problems, the first one was that we were dealing with a very high frequency data collection which contains a large amount of data and allows a high statistical precision on one hand but on the other hand is really computationally expensive to manipulate and to take care of; even so the computational cost is something we can deal with, thanks to modern computers, but the real difficulty lied in the creation of an object that could coherently store our data and on which we could apply the studied methods. Unfortunately we were not able to do so.

Moreover, the second problem consisted in the finding of multiple seasons. In fact, our data presents a daily, a \textit{monthly} and a yearly seasonality. With the models studied so far we were not able to deal with multiple seasons and this is mainly why we decided to transform our series. We decided to take the daily maximum observations, doing this we actually removed the daily component from our series and we remained with the other two. Going forward  with our analysis we discovered that the \texttt{R} implementations of the ARIMA functions we studied are not able to deal with a frequency higher than 350, for this reason our analysis will only consider the \textit{monthly} seasonality. 

Because of the problems presented above, the analysis we will present will be performed on the dataset with the daily maximum observations considering the \textit{monthly} frequency, even if we know results will not be optimal since usually series on maximum values do not satisfy the normality assumption. In fact, the theory of extreme values states that often the distribution of the maximum values of a sample is a generalised extreme value distribution. Consequently  the new goal of this project will be trying to carry on an analysis on this dataset and, once discovered its limitations, explain the issues in detail and propose some manageable solution and future work.

\section{Methodology}

Before detailing the used methodology we will present to the reader a first exploratory analysis where we started to identify some main component of our series, then we will present the methods used to perform the decomposition of the series and finally we will explain how we proceeded as far as parameter estimation is concerned.

\subsection{A First Exploratory Analysis}

Even if at this point we are dealing with daily observations, we still have a great volume of data (i.e. daily observations for 36 years) which made it impossible to identify any pattern on the whole dataset; this is why we are going to separate it in smaller intervals.\\

\noindent Firstly we extracted from different periods 5 years subsets of observations and for each subset we computed the mean, from 1983 to 1988 mean is 61.79, from 1998 to 2003 mean is 65.88 and from 2013 to 2018 mean resulted to be 70.83. We see we are dealing with an increasing mean that will probably suggest the presence of a positive trend.\\

\noindent Secondly we plotted these three subsets [\ref{fig:1}] and we noticed a certain yearly seasonality, confirmed by the fact that the sun's declination affects tides as a matter of fact according to the season high tides are more or less probable \cite{noaa}. Unfortunately even if we were able in practice to decompose the series with frequency set to 365\footnote{This decomposition will not be shown for reasons of space, but we used the same methods of the decomposition we will present in the next subsection with different parameters} we were not able to fit a (S)ARIMA model for the reason explained above.\\

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/5yearssubsetsobservations.png}
\caption{5 years subset observations}
\label{fig:1}
\end{figure}

\noindent Finally, we decided to take three different periods (from different years) of three months [\ref{fig:2}], to see if we could deduce something more.

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/3monthssubsetsobservations.png}
\caption{3 months subset observations}
\label{fig:2}
\end{figure}

\noindent In fact, from these plots we can notice something that resembles a monthly frequency, in particular, if we look with more attention we will find out that this frequency is actually a little bit less than a month, and if we recall some general knowledge on tides we will consider that they follow the lunar phases and a whole cycle of lunar phases lasts 28 days each; this is why we choose 28 as the frequency of our series.

\subsection{Decomposition}

When choosing a forecasting method we will first need to identify the time series patterns in the data and then choose a method that is able to capture the patterns properly. This is why we are going to  perform decomposition, to have an idea of what kind of properties a model will need in order to be a good representation of our data.\\

We assumed an additive model for seasonality i.e. $y_t=m_t+s_t+e_t$ \footnote{Where $y_t$ is our time series, $m_t$ is the trend, $s_t$ is the seasonality and $e_t$ are the residuals} because as we can see from the series plot [\ref{fig:3}] we do not see oscillations (representing seasonality) growing with trend.

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/wholeseriesplot.png}
\caption{Daily Maximum Tides, whole time series}
\label{fig:3}
\end{figure}

At this point we used the function \texttt{decompose()} to have a reference (and correct) decomposition, then we tried to do it manually and to compare what we did with the reference one. This was done to be sure we understood properly how the function \texttt{decompose()} works and also the behaviour of the time series since, as we will see, in order to extract the different components manually we need to deeply understand the structure of the data. \\

First of all we applied a moving average symmetric filter, this is a low-pass filter; as a matter of fact low-pass filters are used for trend extraction and identification. At each time point we average over an interval centred at $t$. How many observations we consider in this average is given by the smoothing parameter $p$, that we choose to be 14 consequently we will consider 14 observations before a certain point and 14 after the same point. The value of the smoothing parameter was chosen to be 14 because in generating the function (i.e. the filter) that will capture the trend we want to be sure that every estimated point takes into consideration a whole seasonal cycle.

Notice that, in order to perform smoothing, initial and final parts of the series will be missing because we need those observations in order to compute the value of the filter in between. We show the comparison of the two extracted trends and we can appreciate that they match; looking at this graph [\ref{fig:4}] we can see we are dealing with a slight trend as a matter of fact it is highly influenced by some seasonal component and it is not able to hide it.

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/trend.png}
\caption{Trend}
\label{fig:4}
\end{figure}

Since we saw a lot of seasonality in the previous trend we applied again a MA filter but this time it will be ad hoc for seasonal data, in fact, it is going to consider seasonal lags computing the MA from the first day of the lunar phase to the last one and then shifted from the second one to the first of the next lunar phase. This filter will allow us to identify a trend that takes into consideration also the seasonal effect we specified.

\noindent As we can see from the following graph [\ref{fig:5}], our trend still presents an high seasonal effect, this is due to the fact we are not considering the other seasonal component which is probably more relevant. 

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/seasonaltrend.png}
\caption{Seasonal Trend}
\label{fig:5}
\end{figure}


When the trend is not the most relevant component it is usually good practice to estimate the seasonal component, which is what we are going to do below. To recover the seasonal structure we removed the second discovered trend from the original series in order to expose seasonality and compute the estimated seasonal figure\footnote{In the reference decomposition, this estimated seasonal figure will be contained in \texttt{figure}, if we repeat this pattern for the number of times we expect to see it in the whole series we will have the whole seasonal pattern contained in \texttt{seasonal}. }. To do so we take our detrended time series and we transform it in a matrix, then we take the transpose of this matrix so that each column will contain elements from the same period and we compute the mean of each column.

As we can appreciate from the following graph [\ref{fig:6}] the procedure to extract the correct seasonal figure was correct however we notice that this seasonality is not really relevant. This is due to the fact we are considering daily maxima; which do not represent the natural course of tides\footnote{Maybe to better appreciate the lunar cycle seasonality we should consider daily mean since the mean summarizes information better than the maximum. However doing this it would be practically impossible to predict the rare events we were interested about.}. Another proof the 28-days seasonality is not so much relevant is given by the Mean Absolute Percentage Error computed on the two found trends, which value is approximately 0.5\% which means the two trends are almost the same whether they consider the lunar cycle or not.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{Images/seasonalfigure.png}
\caption{Seasonal figure}
\label{fig:6}
\end{figure}

We are now able to extract the residuals removing the trend that ignores the 28-days seasonality and the seasonal figure repeated 470 times\footnote{Approximately this is the number of times a cycle repeats in 36 years.}.
From figure [\ref{fig:7}] we can see there is still a lot of information left, probably the information carried out from autoregressive components and the seasonality we are not considering.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{Images/residuals.png}
\caption{Residuals}
\label{fig:7}
\end{figure}

\subsection{Parameter Estimation}

At this point we need to choose a model that fairly represents our data, models allow us to generalise what we observe in the data to the general phenomenon and allow us to do proper forecasting in a probabilistic manner, meaning we aim to be able to quantify uncertainty.\\
Consequently we need to choose our model; to do so we need to analyse the series autocorrelation and partial autocorrelation, because we know for AR processes of order $p$ the ACF is exponentially decaying or sinusoidal whereas the PACF is zero beyond lag $p$; conversely, for MA processes of order $q$ the ACF is zero beyond lag $q$ and the PACF is exponentially decaying or sinusoidal. Moreover, we would like to see the effect of a first difference and also of a first seasonal difference since we are distant from having a constant mean process. Unfortunately ACF and PACF plots, as we will soon see, become more difficult to read when we are dealing with more complicated processes (i.e. not pure AR or MA), for this reason we will only be able to  choose some candidates values as model orders and then we will use some criteria to choose the best candidate.\\

Below we will first show the plots of the first seasonal and non-seasonal difference [\ref{fig:8}] of our time series to be able to explain the first candidate models we decided to try. Then we will explain how we decided  if a candidate model was actually valid or not and we will also present a summary table with the best models we found\footnote{We will cite Box tests done in the code and considerations on plots that we will not show but that can be seen in the code file. Consider for each candidate we have analysed the ACF and the PACF of the residuals. Moreover on the code file there all the models we tried, here we report just the ones giving the best values.}.

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/firstdifferences.png}
\caption{First seasonal and non-seasonal difference}
\label{fig:8}
\end{figure}

From the first non-seasonal difference we can decide to set $d=1$ since it makes our data with constant mean. For the moment we do not consider the first seasonal difference and we analyse the ACF and PACF functions of the differenced series [\ref{fig:9}].

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/ACFandPACFdifferencedseries.png}
\caption{(Partial) Autocorrelation function of the differenced time series}
\label{fig:9}
\end{figure}



If we consider the negative peaks in the PACF function we could try models with a non-seasonal AR component of order 1, 2, 3 or maybe even 4. Moreover as far as the MA component is concerned we can look at the ACF and suppose to have a seasonal and a non-seasonal components, both of order 1. So these will be the first fitted models; notice that for the moment to evaluate the models we fit them on the whole series. 

To summarize, we already said why we choose to try the first four models\footnote{ARIMA(1,1,1)(0,0,1)[28], ARIMA(2,1,1)(0,0,1)[28], ARIMA(3,1,1)(0,0,1)[28], ARIMA(4,1,1)(0,0,1)[28] }, then looking at the ACF \texttt{fit2} residuals we considered to raise of one order the MA seasonal component, so we took the two best models among the first four and we increased $Q$ by one. \texttt{fit5} resulted to be the best one up to that moment, so we decided to try it again setting the order of the AR seasonal component to 1, we made this decision on the basis of the ACF function of the \texttt{fit3} residuals. The set of \textit{good}  models was made by \texttt{fit3}, \texttt{fit4}, \texttt{fit5} and \texttt{fit8}, so we decided to evaluate this models adding a first seasonal difference component since this operation seemed to make our data a little bit better, however the only model worth keeping among the four new was \texttt{fit12}\footnote{Remember we cannot compare the last four models with the previous ones because we have a different seasonal order of differencing.}. \\
\newpage
To evaluate the best ones among these five we decided to rely on two methods. Firstly, when possible\footnote{We can compare in terms of information criteria only if we are using the exact same data.}, we considered the corrected Akaike Information, one of the studied  information criterion to measure how much information is contained in the data, regarding the parameters but also penalizing for the number of parameters\footnote{The reason why we need to penalize for the number of parameters is that we want enough parameters to fit data well but not too many that we end up overfitting. Moreover having too many parameters makes interpretation harder.}.  In practice we are measuring  the information contained in the observations but making sure we are not wasting it estimating too many parameters. Notice that when evaluating  this measure we prefer models with lower values.\\

In addition, we considered both the studied portmanteau tests, which control if residuals are significantly different from what we would be expecting from a white noise process. We performed both Box-Pierce and Box-Ljung tests, the difference between these two is that the second one is more accurate since it gives a role to the lag. To consider autocorrelations close to zero i.e. coming from a white noise process we want the values of these tests to be as small as possible. In practice we reject the Null hypothesis (i.e. residuals are white noise) if the p-value is less than 0.05. When we do not reject the null it means residuals are white noise therefore the model is good enough to proceed with forecasting.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
$p$ & $d$ & $q$ & $P$ & $D$ & $Q$ & Name in the code                         & Reasons why                                                                                                                                      \\ \hline
3   & 1   & 1   &     &     & 1   & \textbf{\texttt{fit3}}  & \begin{tabular}[c]{@{}c@{}}AICc = 95394.97\\ Very good p-values (0.97)\\ Improvement in the p-value\end{tabular}                                 \\ \hline
4   & 1   & 1   &     &     & 1   & \textbf{\texttt{fit4}}  & \begin{tabular}[c]{@{}c@{}}AICc = 95393.34\\ Very good p-values (0.95)\\ Slight improvement in the AICc\end{tabular}                             \\ \hline
2   & 1   & 1   &     &     & 2   & \textbf{\texttt{fit5}}  & \begin{tabular}[c]{@{}c@{}}AICc = 95358.08\\ Very good p-values (0.97)\\ Improvement wrt \texttt{fit3} in the AICc\end{tabular} \\ \hline
2   & 1   & 1   & 1   &     & 2   & \textbf{\texttt{fit8}}  & \begin{tabular}[c]{@{}c@{}}AICc = 95360.07\\ Very good p-values (0.97)\\ No improvement but good as some others\end{tabular}        \\ \hline
2   & 1   & 1   & 1   & 1   & 2   & \textbf{\texttt{fit12}} & \begin{tabular}[c]{@{}c@{}}AICc = 95354.03\\ Very good p-values (0.95) \\ Lowest AICc\footnotemark and high enough p-values\end{tabular}                    \\ \hline
\end{tabular}
\end{table}


\footnotetext{Among the four models we tried with D set to 1.}

After this analysis we decided to keep only the two best models among the five "good enough", and they were \texttt{fit5} and \texttt{fit12}. To choose just one model between these two we split our dataset in training set (first 30 years) and test set (last 6 years) and we fitted again the two models in order to evaluate normality of the residuals, their closeness to be a white noise process and also their forecast capability\footnote{Notice that we are not going to show all the graphs, test results and accuracy measures, but we will assume the reader has access to the file code if he wants to reproduce them.}. \\

Both models residuals qqPlots\footnote{The quantile-quantile (q-q) plot is a graphical technique for determining if two data sets come from populations with a common distribution. A q-q plot is a plot of the quantiles of the first data set against the quantiles of the second data set\cite{qq}.} present a substantial departure from normality, but we already expected this since we know we lack of the normality assumption. 

The two partial autocorrelation functions of the residuals are really bad looking since they are far from being inside the bands meaning we cannot consider them to be zero, meaning we still have some correlation left in our data we are not considering. Unfortunately we did not find a way to make them look better and this is probably because we are not dealing with the native multiple seasonalities of the series.
To choose just one of them we could not rely on the corrected Akaike information since we are applying the models on different datasets (\texttt{fit12} provide also a seasonal difference on the data) consequently we had to rely on he Box tests p-values and on the forecast accuracy measures.

Forecast accuracy measures are actually not that  bad for neither of the two models but this is because forecasts are close to the mean, and this is essentially the same reason why the confidence bands are not that bad if confronted with the observations of the test set. In fact, from the forecasts graphs [\ref{fig:10}] we can see that with both models the majority of the observations of the test set are inside the error bands.

\begin{figure}[H]
\centering
\includegraphics[width=0.75\textwidth]{Images/forecastsfit5trandfit12tr.png}
\caption{Forecasts produced by models \texttt{fit5} and \texttt{fit12}}
\label{fig:10}
\end{figure}
\newpage
Once made the above considerations and looked at the graphs above we decided to keep the model \texttt{fit12}  because measures of accuracy are not that different and its p-value is slightly bigger meaning residuals are closer to be white noise; moreover if we look closer at \texttt{fit12} forecasts [\ref{fig:11}]
we can notice that we have a better defined trend and we have some more oscillation with respect to forecasts of \texttt{fit5} that are, instead, flat. Even so, forecasts are not that good and this is probably due to the fact that the model is considering the 28-days seasonal component but it just does not seem to be relevant in predicting future. In fact, future predictions seem to ignore a more important seasonal structure.\\

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/highlightwigliness.png}
\caption{Zoomed forecasts produced by model \texttt{fit12}}
\label{fig:11}
\end{figure}

Aiming to improve the normality of the residuals we decided to apply Box-Cox transformation to our data, consequently we needed to choose the $\lambda$ parameter and to do so we use the function \texttt{BoxCox.lambda()} which suggests us the value $\approx 0.62$. This makes sense since we know we would like a $\lambda<1$ for positively skewed data which is our case.\\

We performed again\footnote{We need to repeat all the procedure because we changed the data on which we are trying to fit a model.} the model selection procedure exactly as before and since ACF and PACF graphs both of the transformed and of the non-transformed data almost correspond at each step (i.e. every time we fit a model and we check autocorrelation and partial autocorrelation functions  of the residuals) we end up trying the same models.\\

This time the best two models were \texttt{fit5cox} and \texttt{fit8cox}\footnote{These are the names of the models in the code to distinguish them from the previous ones but they actually have the same orders.}; in order to choose just one of the two we are going to apply the models only on the training set and to see which one presents better residuals and forecast capability. The latter is evaluated computing accuracy measures which in this case were similar for both models, however the resulting p-value from the Box tests is better for \texttt{fit8cox}; consequently we choose it to be our best model.\\

Analysing with more precision the residuals we notice that the qqPlot has slightly improved [\ref{fig:12}] but not enough to provide substantially better forecasts as we can see from the plot below\footnote{On the contrary, they have become a little worse, probably because we have the seasonal order of the autoregressive component set to 0.} [\ref{fig:13}]. Finally the partial autocorrelation function is still really far from falling inside  the  \textit{zero bands} and this confirms the hypothesis  for which we are not accounting for all the present seasonal components. 

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{Images/residualsnormality.png}
\caption{Normality has slightly improved}
\label{fig:12}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=0.7\textwidth]{Images/coxforecasts.png}
\caption{Forecasts have not.}
\label{fig:13}
\end{figure}





\section{Limitations and Future Works}
In all the showed forecasts we are clearly failing to account for the native multiple seasonal structure of  the series. We are now going to propose some possible solutions.\\

Firstly, we could try a harmonic regression approach where the seasonal pattern is modelled using Fourier terms, in this way any length seasonality is accepted, moreover Fourier terms of different frequencies can be included to account for multiple seasonal patterns; as a matter of fact the smoothness of these patterns can be controlled by the number of Fourier sine and cosine pairs whereas the trend is still handled by an ARMA model. However we should highlight that Fourier regression would not solve in any case all of our problems since, except for the normality problem, it assumes seasonality as fixed, meaning that the seasonal patterns should not change over time, which is usually the case of long time series such as ours.\\

Secondly, among the problems cited in this report we should add the fact that the models we used usually do not work well with very long time series mostly because the time series behaviour tend to change over time and optimisation of the parameters becomes more time consuming because of the number of observations involved. To solve this problem we could cut-off the earliest observations and use only the more recent ones.\\

Thirdly, to completely solve the problem of multiple evolving seasonality we could explore the TBATS model approach which uses a combination of Fourier terms with an exponential smoothing state space model and a Box-Cox transformation, in a completely automated manner. In fact, in a TBATS model seasonality is allowed to slowly change over time, the only disadvantage left would be the computational time required  to deal with a long series like ours using this kind of model.\\

Moreover, we still have the normality assumption problem, for this kind of issue we should consider an approach based on generalized linear models (GLM)  which are an effective way to deal with time series that have errors distribution different from the normal one. In practice these models are a flexible generalization of ordinary linear regression, they generalize linear regression by allowing the linear model to be related to the response variable via a link function\footnote{The link function provides the relationship between the linear predictor and the mean of the distribution function.} and by allowing the magnitude of the variance of each measurement to be a function of its predicted value.\\

Furthermore, if none of the approaches proposed above give satisfactory results we could try to change perspective and perform a major change in the original dataset: since we wanted to estimate rare events we could consider in our dataset only the events out of the norm, e.g. we could consider only observations that exceed 90 cm. However in this case we would be dealing with a point process consequently we should transform the data, maybe by taking time between one observation and the other to be able to transform it in a time series, at this point we could change our aim in forecasting the time between two rare events (i.e. very high tides).\\

Finally, we must say that there exist other physical variables that should be considered in estimating tides such as wind speed and direction, temperature and whether conditions in general, besides, of the the astronomical influences we have already cited. In addition, we should notice that also climate change plays an important role in the increasing level of tides as a matter of fact global sea level has been rising \cite{noaa2} and the two main causes need to be searched in the thermal expansion caused by the warming of the oceans\footnote{Water expands as it warms.} and the increased melting of land-based ice. These  are all factors that an accurate model should take into consideration.\\

In conclusion, even if we are not completely satisfied with the obtained results we surely were able to practically see which are the limits of certain approaches and this gave us the opportunity to explore (even if superficially) other possibilities. In addition, we should recall that usually for tides forecast besides harmonic analysis (which still requires a large number of parameters) artificial neural networks are used since they can easily decide the unknown parameters by learning the interrelation between the observations we have and the short-term predictions; in any case they are still not able to perform long-term predictions in the future, this is probably why our initial goal was actually not realistic with the means we have nowadays.

\nocite{ts,ts2}
\newpage

\addcontentsline{toc}{section}{References}
\bibliographystyle{ieeetr}
\bibliography{biblio}




\end{document}